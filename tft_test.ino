const bool debug = false;
#define debugSerial (debug && Serial)

#include "mTFT.h"

// VCC-GND      -CS-RESET-AO-SDA-SCK-LED
// (V)、ground(G)、D7、D6、D5、D4、D3、D2
#define LCD_BL  2
mTFT<mtft::TFT01_18SP<4, 3, 7, 6, 5>> mlcd;
CSmallFont font;
int t = 0;
int w, h;

int score = 0;

void setup() {
  // I think does something with a "LED"?
  pinMode(LCD_BL, OUTPUT);
  digitalWrite(LCD_BL, HIGH);

  pinMode(A5, INPUT);

  if (debug) { Serial.begin(9600); while (!Serial); }

  mlcd.InitLCD(PORTRAIT);
  mlcd.clrScr();
  w = mlcd.getDisplayXSize();
  h = mlcd.getDisplayYSize();
  mlcd.setBackColor(VGA_TRANSPARENT);
}

// [0] [header] [1]
// [2]   [3]    [4]
const int adc_key_val[5] = { 20, 50, 100, 200, 600 };
const word colors[5] = { VGA_RED, VGA_GREEN, VGA_BLUE, 0x7be0, VGA_PURPLE };
int NUM_KEYS = 5;

void loop() {
  delay(100);
  int a = analogRead(A5);
  if (a < 700) {
    word c = VGA_WHITE;
    uint8_t key = 9;
    for (int i = 0; i < NUM_KEYS; i++) {
      if (a < adc_key_val[i]) {
        if (debugSerial) Serial.println(i);
        c = colors[i];
        key = i;
        break;
      }
    }
    if (c == VGA_WHITE && debugSerial) {
      Serial.print("Unknown key ");
      Serial.println(a);
    }
    mlcd.fillRect(0, 0, w - 1, h - 1, c);
    mlcd.setColor(VGA_WHITE);
    font.printChar(mlcd, '0' + key, 0, 0);
  }
}
