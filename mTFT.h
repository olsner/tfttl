/*
 * Based on code from UTFT, but heavily modified in places.
 * (http://www.rinkydinkelectronics.com/library.php?id=51)
 *
 * Since UTFT is licensed under creative commons CC-BY-NC-SA 3.0, this code is
 * too.
 *
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
 */

#define FORCEINLINE __attribute__((always_inline))

namespace {

template <typename T>
void swap(T& i, T& j)
{
    T t = i;
    i = j;
    j = t;
}

namespace mtft {

// NB! When converting drivers from UTFT, only use the P_RS name for the
// parallel modes, UTFT set P_RS at runtime for 5-pin serial and its code
// needs to be converted to use e.g. P_SPI_DCX for the data/command pin.
#define P_CS pin_reg<CS>()
#define B_CS pin_mask<CS>()
#define P_RST pin_reg<RST>()
#define B_RST pin_mask<RST>()

// For parallel interfaces (register select and write?)
#define P_RS pin_reg<RS>()
#define B_RS pin_mask<RS>()
#define P_WR pin_reg<WR>()
#define B_WR pin_mask<WR>()

// Alias for latched 16-bit
#define P_ALE pin_reg<ALE>()
#define B_ALE pin_mask<ALE>()

// For serial interfaces
#define P_SDA pin_reg<SDA>()
#define B_SDA pin_mask<SDA>()
#define P_SCL pin_reg<SCL>()
#define B_SCL pin_mask<SCL>()
// Data/Command pin for 5-pin SPI
#define P_SPI_DCX pin_reg<DCX>()
#define B_SPI_DCX pin_mask<DCX>()

// Template code and documentation for the generic HW interface...
// This should be implemented for each specific platform (e.g. Arduino Uno).
namespace hw {
#if 0
    // Opaque types used for hardware register addresses/indexes.
    typedef uint8_t regsize;
    typedef volatile regsize regtype;
    typedef unsigned int* bitmapdatatype;

    // These are predefined in AVR but would be needed for other architectures,
    // refactor to actually go through hw:: for these.
    // Note the pgm_read_byte name is a macro, so we need to rename it then.
    uint8_t pgm_read_byte(const uint8_t *p) {
        return *p;
    }
    uint16_t pgm_read_word(const uint16_t *p) {
        return *p;
    }

    // Replace these with model-specific static code. No need to hike all the
    // way to an array in program memory for static configuration.
    // The result should be equivalent to the corresponding function calls in
    // the arduino libs.
    template <int PIN> regtype *pin_reg()
    {
        return portOutputRegister(digitalPinToPort(PIN));
    }
    template <int PIN> regsize pin_mask()
    {
        return digitalPinToBitMask(PIN);
    }

    // TODO There needs to be a hardware-specific init for SAM3X8E. Though
    // there is already a transport init in each struct.

    // Each interface style should have its own struct template for each
    // device, this replaces the switch in each HW's LCD_Writ_Bus in UTFT.
    // The LCD-model-specific code does e.g. hw::SPI4<x,y> where 'hw' is
    // typedef:ed to the current hardware's implementation, and gets a
    // hw-specific SPI4 implementation connected to the right pins.
    //
    // The LCD model templates take enough pins to instantiate its required
    // interface.
    template <int SDA, int SCL> struct SPI4;
    template <int SDA, int SCL, int DCX> struct SPI5;
    template <int RS, int WR> struct par8;
    template <int RS, int WR> struct par16;
    template <int RS, int WR, int CS, int ALE> struct latched16;
#endif
} // namespace mtft::hw

} // namespace mtft

// Start of AVR code
// FIXME Move to AVR-specific file.

// Could run into trouble including these inside the anonymous namespace.
#include <avr/io.h>
#include <avr/pgmspace.h>

namespace mtft {

namespace hw {
    typedef uint8_t regsize;
    typedef volatile regsize regtype;
    typedef unsigned int* bitmapdatatype;

    void cbi(regtype* reg, regsize bitmask) {
        *reg &= ~bitmask;
    }
    void sbi(regtype* reg, regsize bitmask) {
        *reg |= bitmask;
    }

    void pulse_high(regtype* reg, regsize bitmask) {
        sbi(reg, bitmask);
        cbi(reg, bitmask);
    }
    void pulse_low(regtype* reg, regsize bitmask) {
        cbi(reg, bitmask);
        sbi(reg, bitmask);
    }

    void cport(regtype& port, regsize data) {
        port &= data;
    }
    void sport(regtype& port, regsize data) {
        port |= data;
    }
}

// Start of ATM328P code
// FIXME Move to ATM328p-specific file. This is probably Uno specific
// too right now, since the ATM328p pins could be assigned differently.
namespace ATM328p_hw
{
    using namespace hw;

    template <int PIN> regtype *pin_reg()
    {
        static_assert(PIN < 14, "Only 14 digital pins available on ATM328");
        if (PIN < 8) return &PORTD;
        if (PIN < 14) return &PORTB;
    }

    template <int PIN> regsize pin_mask()
    {
        // pins 0..7 are bits 0..7 on port D
        // pins 8..13 are bits 0..5 on port B
        static_assert(PIN < 14, "Only 14 digital pins available on ATM328");
        return 1 << (PIN & 7);
    }

    template <typename transport>
    void generic_fill_16_8(uint8_t ch, uint8_t cl, size_t n)
    {
        while (n--) {
            transport::write_data(ch);
            transport::write_data(cl);
        }
    }

    template <int SDA, int SCL> struct SPI_gen
    {
        // UTFT uses pulse_low, but Adafruit uses pulse_high. What gives?
        static void clock()
        {
            pulse_low(P_SCL, B_SCL);
        }

        static void write_raw(uint8_t VL)
        {
            // Makes 0-fills noticably faster. Might want specialized fill
            // functions for data that is all 0 later.
            if (!VL) {
                cbi(P_SDA, B_SDA);
                clock();
                clock();
                clock();
                clock();
                clock();
                clock();
                clock();
                clock();
                return;
            }

#define BIT(mask) \
            do { \
                if (VL & mask) \
                    sbi(P_SDA, B_SDA); \
                else \
                    cbi(P_SDA, B_SDA); \
                clock(); \
            } while (0)

            // Since we're branching and testing anyway, we might put an early
            // out when all remaining bits are 0 here.
            BIT(0x80);
            BIT(0x40);
            BIT(0x20);
            BIT(0x10);
            BIT(0x08);
            BIT(0x04);
            BIT(0x02);
            BIT(0x01);

#undef BIT
        }
    };

    // Specialization when connecting the display to the SPI pins, should be
    // much faster. Chip select is controlled further up the stack, and will
    // be pulled low when we're writing.
    // TODO May or may not be safe to use together with the SPI4 variant,
    // where there's an extra bit before each byte.
    // TODO Have some way to set the clock depending on the LCD model.
    // Perhaps this should be a separate interface (otoh, ability to use hwSPI
    // is up to the LCD and the platform and how the user connects it all up).
    // TODO Fix this and make it actually work :)
    template <> struct SPI_gen<11 /* MOSI */, 13 /* SCK */>
    {
        enum {
            SDA = 11,
            SCL = 13,
        };

        // Used for SPI4 sending the command/data bit
        static void clock()
        {
            pulse_low(P_SCL, B_SCL);
        }

        static void write_raw(uint8_t VL)
        {
            // Should we disable power savings for SPI here, or do it on init
            // and rely on it being on? Perhaps assume it is on, and require
            // that the application enable/disable and enable before doing any
            // display stuff.
            //
            // If you wanted to save power you'd be putting the display in
            // power save too...

            // pin modes are already set up the same as if we used bit banging,
            // we'll just let the SPI do the banging instead of us.


            SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR1) | (1 << SPR0);
            // SPR1:0 = clock divisor selection
            //   0..3 selects f / (4, 16, 64, 128)
            //   SPI2X in SPSR doubles the clock (halves the divisor)
            // CPHA,CPOL = 0,0
            // In the normal bit banging path, pulse_low does low then high, so
            // that's a rising edge after we've safely set the data pin. This
            // should make the SPI hardware do the same.
            // MSTR = 1: We're the master
            // DORD = 0: MSB first
            // SPE = 1: Enable
            // SPIE = 1: Not interrupts, we'll block until it's done

            // Disable SPI2X (also touches SPIF and WCOL).
            SPSR = 0;

            SPDR = VL;
            while (!(SPSR & (1 << SPIF)));
            // read to discard input data and clear SPIF
            (void)SPDR;

            // Disable again
            SPCR = 0;
        }
    };

    // TODO Split up command/data flag write and data write, VH argument is
    // almost never a variable.
    // It's only a variable when doing 16-bit writes in parallel-8 or
    // LATCHED_16 mode, in UTFT::fillScr, when fill_8 can't be used.

    template <int SDA, int SCL> struct SPI4: private SPI_gen<SDA, SCL>
    {
        using SPI_gen<SDA, SCL>::clock;
        using SPI_gen<SDA, SCL>::write_raw;

        static void write(uint8_t VH, uint8_t VL)
        {
            if (VH==1)
                sbi(P_SDA, B_SDA);
            else
                cbi(P_SDA, B_SDA);
            clock();

            write_raw(VL);
        }

        static void write_data(uint8_t VL)
        {
            write(0x01,VL);
        }

        static void write_cmd(uint8_t VL)
        {
            write(0x00,VL);
        }

        static void fill_16(int ch, int cl, long pix)
        {
            generic_fill_16_8<SPI4<SDA, SCL>>(ch, cl, pix);
        }
    };

    template <int SDA, int SCL, int DCX> struct SPI5: private SPI_gen<SDA, SCL>
    {
        using SPI_gen<SDA, SCL>::write_raw;

        static void init()
        {
            pinMode(SDA, OUTPUT);
            pinMode(SCL, OUTPUT);
            pinMode(DCX, OUTPUT);
        }

        static void write(uint8_t VH, uint8_t VL)
        {
            if (VH==1)
                write_data(VL);
            else
                write_cmd(VL);
        }

        static void write_data(uint8_t VL)
        {
            sbi(P_SPI_DCX, B_SPI_DCX);
            write_raw(VL);
        }

        static void write_cmd(uint8_t VL)
        {
            cbi(P_SPI_DCX, B_SPI_DCX);
            write_raw(VL);
        }

        static void write_data(uint8_t VH, uint8_t VL)
        {
            sbi(P_SPI_DCX, B_SPI_DCX);
            write_raw(VH);
            write_raw(VL);
        }

        static void fill_8(uint8_t c, size_t n)
        {
            sbi(P_SPI_DCX, B_SPI_DCX);
            while (n--) write_raw(c);
        }

        static void fill_16(int ch, int cl, size_t n)
        {
            if (ch == cl) {
                fill_8(ch, 2 * n);
                return;
            }
            sbi(P_SPI_DCX, B_SPI_DCX);
            while (n--) {
                write_raw(ch);
                write_raw(cl);
            }
        }
    };

    // TODO Make the amount of unrolling configurable for speed/size tradeoff
    template <int WR>
    void pulse_WR_low(size_t n)
    {
        size_t blocks = n/16;
        for (size_t i=0; i<blocks; i++)
        {
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
            pulse_low(P_WR, B_WR);
        }
        if ((n % 16) != 0)
            for (size_t i=0; i<(n % 16)+1; i++)
            {
                pulse_low(P_WR, B_WR);
            }
    }

    template <int RS, int WR>
    struct par16
    {
        static void init()
        {
            DDRD = 0xFF;
            DDRB |= 0x3F;
            DDRC |= 0x03;
        }

        static void write(uint8_t VH, uint8_t VL)
        {
            PORTD = VH;
            cport(PORTC, 0xFC);
            sport(PORTC, (VL>>6) & 0x03);
            PORTB =  VL & 0x3F;
            pulse_low(P_WR, B_WR);
        }

        static void fill_16(int ch, int cl, long pix)
        {
            sbi(P_RS, B_RS);

            PORTD = ch;
            cport(PORTC, 0xFC);
            sport(PORTC, (cl>>6) & 0x03);
            PORTB = cl & 0x3F;

            pulse_WR_low<WR>(pix);
        }

        static void write_cmd(uint8_t VL)
        {
            cbi(P_RS, B_RS);
            write(0x00,VL);
        }

        static void write_data(uint8_t VL)
        {
            sbi(P_RS, B_RS);
            write(0x00,VL);
        }

        static void write_data(uint8_t VH, uint8_t VL)
        {
            sbi(P_RS, B_RS);
            write(VH, VL);
        }
    };

    template <int RS, int WR>
    struct par8
    {
        static void init()
        {
            DDRD = 0xff;
        }

        static void write(uint8_t VH, uint8_t VL)
        {
            PORTD = VH;
            pulse_low(P_WR, B_WR);
            PORTD = VL;
            pulse_low(P_WR, B_WR);
        }

        static void fill_8(int ch, long n)
        {
            sbi(P_RS, B_RS);

            PORTD = ch;
            par8::pulse_WR_low(n);
        }

        static void fill_16(int ch, int cl, long pix)
        {
            if (ch == cl)
                fill_8(ch, 2 * pix);
            else
                generic_fill_16_8<par8<RS, WR>>(ch, cl, pix);
        }
    };

    template <int RS, int WR, int CS, int ALE>
    struct latched16: par16<RS, WR>
    {
        static void init()
        {
            par16<RS, WR>::init();
            cbi(P_ALE, B_ALE);
            pinMode(8,OUTPUT);
            digitalWrite(8, LOW);
        }

        static void write(uint8_t VH, uint8_t VL)
        {
            PORTD = VH;
            cbi(P_ALE, B_ALE);
            pulse_high(P_ALE, B_ALE);
            cbi(P_CS, B_CS);
            PORTD =  VL;
            pulse_low(P_WR, B_WR);
            sbi(P_CS, B_CS);
        }
    };
}

namespace hw {
    using namespace ATM328p_hw;
}

// End ATM328P code

} // namespace mtft

// End of AVR code

namespace mtft {

#define pin_reg hw::pin_reg
#define pin_mask hw::pin_mask

template <typename T>
void write_script(const uint8_t* progmem_data)
{
    // len includes command byte to support commands with no data.
    while (uint8_t len = pgm_read_byte(progmem_data++))
    {
        T::write_cmd(pgm_read_byte(progmem_data++));
        // run len-1 times
        while (--len) T::write_data(pgm_read_byte(progmem_data++));
    }
}

template <int SDA, int SCL, int CS, int RST, int SDX>
struct ST7735S
{
    typedef hw::SPI5<SDA, SCL, SDX> transport;

    enum {
        width = 128,
        height = 160,
    };

    // Small screen only needs byte-width coordinates
    // Separate types so that if only one dimension exceeds 256, the other can
    // still use shorter types.
    typedef uint8_t X;
    typedef uint8_t Y;

    static void init_transport()
    {
        pinMode(CS,OUTPUT);
        pinMode(RST,OUTPUT);
        transport::init();
    }

    static void write_data(char VL)
    {
        transport::write_data(VL);
    }
    static void write_cmd(uint8_t c)
    {
        transport::write_cmd(c);
    }

    enum Command {
        // Sleep out and booster on
        C_SLPOUT = 0x11,

        C_DISPON = 0x29,
        // SetX/Y
        C_CASET = 0x2a,
        C_RASET = 0x2b,
        C_RAMWR = 0x2c,

        C_MADCTL = 0x36,
        C_COLMOD = 0x3a,

        // Frame rate controls
        C_FRMCTR1 = 0xb1,
        C_FRMCTR2,
        C_FRMCTR3,

        // display inversion control
        C_INVCTR = 0xb4,

        C_DISSET5 = 0xb6,

        C_PWCTR1 = 0xc0,
        C_PWCTR2,
        C_PWCTR3,
        C_PWCTR4,
        C_PWCTR5,

        C_VMCTR1 = 0xc5,
        C_VMOFCTR = 0xc7,

        // Gamma adjustment (positive polarity)
        C_GAMCTRP1 = 0xe0,
        // Gamma adjustment (negative polarity)
        C_GAMCTRN1 = 0xe1,
    };

    void init_lcd() {
        // Maximum delay from reset to when slpout is accepted.
        delay(120);

        static const PROGMEM uint8_t init_script[] = {

            // Sleep exit
            1, C_SLPOUT,

            //ST7735R Frame Rate

            // All Frame Rate commands take the same three parameters:
            // RTN = 5, FP = BP = 63 (max)
            // frame rate = 333kHz/((RTN + 20) x (LINE + FP + BP))
            // line = number of display lines?
            // Then these values are roughly 47 FPS

            // colors normal mode
            4, C_FRMCTR1,
            0x05, 0x3C, 0x3C,

            // Idle mode
            4, C_FRMCTR2,
            0x05, 0x3C, 0x3C,

            7, C_FRMCTR3,
            // 1st to 3rd parameter set frame rate for line inversion mode
            // 4..6 for frame inversion mode
            0x05, 0x3C, 0x3C, 0x05, 0x3C, 0x3C,

            2, C_INVCTR,
            // bits 0..2 set for color normal / idle / partial mode
            // this disables inversion in full colors normal mode (which I
            // think we use)
            0x03,

            //ST7735R Power Sequence
            // Magic values (these don't seem to match the datasheet for
            // ST7735S at all, but UTFT worked with this setup...)

            4, C_PWCTR1, 0x28, 0x08, 0x04,
            2, C_PWCTR2, 0xC0,
            3, C_PWCTR3, 0x0D, 0x00,
            3, C_PWCTR4, 0x8D, 0x2A,
            3, C_PWCTR5, 0x8D, 0xEE,

            2, C_VMCTR1, 0x1A, // VCOMH = 3.150V
            // ??? There should be a second byte for VCOML here...

            2, C_MADCTL,
            0xC0, //MX, MY, RGB mode

                //ST7735R Gamma Sequence
            17, C_GAMCTRP1,
            0x03, 0x22, 0x07, 0x0A, 0x2E, 0x30, 0x25, 0x2A,
            0x28, 0x26, 0x2E, 0x3A, 0x00, 0x01, 0x03, 0x13,
            17, C_GAMCTRN1,
            0x04, 0x16, 0x06, 0x0D, 0x2D, 0x26, 0x23, 0x27,
            0x27, 0x25, 0x2D, 0x3B, 0x00, 0x01, 0x04, 0x13,

            //5, C_CASET,
            //0x00, 0x00, 0x00, 0x7F,
            //5, C_RASET,
            //0x00, 0x00, 0x00, 0x9F,

            2, C_COLMOD,
            0x05, // 16 bit/pixel

            1, C_DISPON,
            0
        };
        write_script<transport>(init_script);
    }

    void setXY(X x1, Y y1, X x2, Y y2)
    {
        // TODO Maybe use write_script here? (Though write_script assumes
        // program memory is used.)
        write_cmd(C_CASET);
        write_data(0);
        write_data(x1);
        write_data(0);
        write_data(x2);
        write_cmd(C_RASET);
        write_data(0);
        write_data(y1);
        write_data(0);
        write_data(y2);
        // Interpret further data as memory write in sequence
        write_cmd(C_RAMWR);
    }

    // On AVR these expand to single instructions, so should be inlined
    FORCEINLINE void select()
    {
        hw::cbi(P_CS, B_CS);
    }
    FORCEINLINE void deselect()
    {
        hw::sbi(P_CS, B_CS);
    }

    FORCEINLINE void reset()
    {
        hw::cbi(P_RST, B_RST);
    }
    FORCEINLINE void dereset()
    {
        hw::sbi(P_RST, B_RST);
    }
};

template <int SDA, int SCL, int CS, int RST, int SDX>
using TFT01_18SP = ST7735S<SDA,SCL,CS,RST,SDX>;

}

enum Orientation { PORTRAIT };

const uint32_t VGA_TRANSPARENT = -1;
// VGA color palette
const word VGA_BLACK = 0x0000;
const word VGA_WHITE = 0xFFFF;
const word VGA_RED = 0xF800;
const word VGA_GREEN = 0x0400;
const word VGA_BLUE = 0x001F;
const word VGA_SILVER = 0xC618;
const word VGA_GRAY = 0x8410;
const word VGA_MAROON = 0x8000;
const word VGA_YELLOW = 0xFFE0;
const word VGA_OLIVE = 0x8400;
const word VGA_LIME = 0x07E0;
const word VGA_AQUA = 0x07FF;
const word VGA_TEAL = 0x0410;
const word VGA_NAVY = 0x0010;
const word VGA_FUCHSIA = 0xF81F;
const word VGA_PURPLE = 0x8010;

template <typename MODEL>
class mTFT
{
    typedef typename MODEL::transport transport;

    MODEL model;

    // {foreground, background} color {high, low}
    byte fch, fcl;
    byte bch, bcl;
    bool transparent;

public:
    enum {
        width = MODEL::width,
        height = MODEL::height,
    };

    typedef typename MODEL::X X;
    typedef typename MODEL::Y Y;

    mTFT(): fch(0xff), fcl(0xff), bch(0), bcl(0), transparent(false) {
        model.init_transport();
    }

    // orientation support is removed - if you want flipped output there might
    // be a wrapper type for that, otherwise do it yourself.
    void InitLCD(Orientation) {
        model.dereset();
        delay(5);
        model.reset();
        delay(15);
        model.dereset();
        delay(15);

        model.select();
        model.init_lcd();
        model.deselect();
    }

    void clrXY()
    {
        model.setXY(0, 0, width - 1, height - 1);
    }

    uint16_t getDisplayXSize() {
        return width;
    }
    uint16_t getDisplayYSize() {
        return height;
    }

    word rgbWord(uint8_t r, uint8_t g, uint8_t b) {
        return ((r & 0xf8) << 8) | ((g & 0xfc) << 3) | (b >> 3);
    }

    void setColor(uint8_t r, uint8_t g, uint8_t b) {
        fch=((r&248)|g>>5);
        fcl=((g&28)<<3|b>>3);
    }
    void setColor(word color) {
        // fc = color;
        fch=byte(color>>8);
        fcl=byte(color & 0xFF);
    }
    void setBackColor(uint8_t r, uint8_t g, uint8_t b) {
        //bc = ((r & 0xf8) << 8) | ((g & 0xfc) << 3) | (b >> 3);
        bch=((r&248)|g>>5);
        bcl=((g&28)<<3|b>>3);
        transparent = false;
    }

    void setBackColor(uint32_t color)
    {
        if (color == VGA_TRANSPARENT)
            transparent=true;
        else
        {
            bch = color >> 8;
            bcl = color;
            transparent=false;
        }
    }

    void clrScr() {
        fillScr(0);
    }

    void fillScr(word color) {
        fillRect_(0, 0, width - 1, height - 1, color >> 8, color);
    }

    void fillRect(X x1, Y y1, X x2, Y y2, word color) {
        fillRect(x1, y1, x2, y2, color >> 8, color);
    }

    void fillRect(X x1, Y y1, X x2, Y y2) {
        fillRect(x1, y1, x2, y2, fch, fcl);
    }

    void fillRect(X x1, Y y1, X x2, Y y2, uint8_t ch, uint8_t cl) {
        if (x1>x2)
        {
            swap(x1, x2);
        }
        if (y1>y2)
        {
            swap(y1, y2);
        }

        fillRect_(x1, y1, x2, y2, ch, cl);
    }

    // Version that requires (doesn't check), that x1 <= x2 and y1 <= y2
    void fillRect_(X x1, Y y1, X x2, Y y2, uint8_t ch, uint8_t cl) {
        model.select();
        model.setXY(x1, y1, x2, y2);
        size_t n = size_t(x2 - x1 + 1) * (y2 - y1 + 1);
        transport::fill_16(ch, cl, n);
        model.deselect();
    }
    void fillRect_(X x1, Y y1, X x2, Y y2) {
        fillRect_(x1, y1, x2, y2, fch, fcl);
    }

    void drawRect(X x1, Y y1, X x2, Y y2)
    {
        if (x1>x2)
        {
            swap(x1, x2);
        }
        if (y1>y2)
        {
            swap(y1, y2);
        }

        drawHLine(x1, y1, x2-x1);
        drawHLine(x1, y2, x2-x1);
        drawVLine(x1, y1, y2-y1);
        drawVLine(x2, y1, y2-y1);
    }

    void drawHLine(X x, Y y, int l)
    {
        if (l<0)
        {
            l = -l;
            x -= l;
        }
        fillRect_(x, y, x + l, y);
    }

    void drawVLine(X x, Y y, int l)
    {
        if (l<0)
        {
            l = -l;
            x -= l;
        }
        fillRect_(x, y, x, y + l);
    }


    void setPixel(uint8_t ch, uint8_t cl)
    {
        transport::write_data(ch, cl);
    }

    void drawChar(const X x, Y y, const uint8_t *data, uint8_t w, uint8_t h)
    {
        model.select();
        if (!transparent)
        {
            model.setXY(x,y,x+w-1,y+h-1);

            for(uint8_t j = 0; j < (w / 8) * h; j++)
            {
                uint8_t ch = pgm_read_byte(data++);
                for(uint8_t i=0;i<8;i++)
                {
                    if(ch & (1<<(7-i)))
                    {
                        setPixel(fch, fcl);
                    }
                    else
                    {
                        setPixel(bch, bcl);
                    }
                }
            }
        }
        else
        {
            for(uint8_t yy = 0; yy < h; yy++)
            {
                for (uint8_t zz = 0; zz < w / 8; zz++)
                {
                    uint8_t ch = pgm_read_byte(data++);
                    for(uint8_t i=0;i<8;i++)
                    {
                        int xx = x + (zz * 8) + i;
                        if(ch & (1<<(7-i)))
                        {
                            model.setXY(xx, y, xx + 1, y + 1);
                            setPixel(fch, fcl);
                        }
                    }
                }
                y++;
            }
        }

        clrXY();
        model.deselect();
    }

};

class Font
{
    const uint8_t *fontdata;
    uint8_t x_size, y_size, numchars, bytespc;

public:
    Font(uint8_t* font): fontdata(font)
    {
        x_size=pgm_read_byte(&font[0]);
        y_size=pgm_read_byte(&font[1]);
        numchars=pgm_read_byte(&font[3]);
        uint8_t offset = pgm_read_byte(&font[2]);
        bytespc = (x_size / 8) * y_size;
        // Offset so we can index with character*bytespc directly
        fontdata = font + 4 - offset * bytespc;
    }

    template <typename TFT>
    void print(TFT& tft, const __FlashStringHelper *fs, int x, int y, int deg = 0)
    {
        const char *s = (const char *)fs;
        while (char c = pgm_read_byte(s++))
        {
            printChar(tft, c, x, y);
            x += x_size;
        }
    }

    template <typename TFT>
    void print(TFT& tft, const char *st, int x, int y, int deg = 0)
    {
        while (char c = *st++)
        {
            printChar(tft, c, x, y);
            x += x_size;
        }
    }

    template <typename TFT>
    void printChar(TFT& tft, char c, int x, int y)
    {
        tft.drawChar(x, y, fontdata + c * bytespc, x_size, y_size);
    }
};

template <uint8_t DATA[], uint8_t W, uint8_t H, uint8_t NUMCHARS, uint8_t FIRSTCHAR>
class CFont
{
    enum {
        BYTES_PER_CHAR = (W / 8) * H,
        NUL_OFFSET = - FIRSTCHAR * BYTES_PER_CHAR,
        HEADER_SIZE = 4,
    };
public:
    template <typename TFT>
    static void print(TFT& tft, const __FlashStringHelper *fs, int x, int y, int deg = 0)
    {
        const char *s = (const char *)fs;
        while (char c = pgm_read_byte(s++))
        {
            printChar(tft, c, x, y);
            x += W;
        }
    }

    template <typename TFT>
    static void print(TFT& tft, const char *st, int x, int y, int deg = 0)
    {
        while (char c = *st++)
        {
            printChar(tft, c, x, y);
            x += W;
        }
    }

    template <typename TFT>
    static void printChar(TFT& tft, char c, int x, int y)
    {
        tft.drawChar(x, y, DATA + HEADER_SIZE + NUL_OFFSET + c * BYTES_PER_CHAR, W, H);
    }
};

}

extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

// These are compile-time-evaluated (as much as possible) wrappers around
// fonts from UTFT. Saves like 140..180 bytes compared to run-time fonts.
typedef CFont<SmallFont, 8, 12, 95, ' '> CSmallFont;
typedef CFont<BigFont, 16, 16, 95, ' '> CBigFont;
typedef CFont<SevenSegNumFont, 32, 50, 10, '0'> CSevenSegNumFont;
