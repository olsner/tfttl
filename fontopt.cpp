#include <ctype.h>
#include <stdint.h>
#include <stdio.h>

#ifdef linux
#include <algorithm>
using namespace std;

namespace {

#define PROGMEM
typedef const uint8_t fontdatatype;

#include "DefaultFonts.c"

char toprint(char c) {
    return isprint(c) ? c : '.';
}

typedef uint8_t u8;

bool is_blank(const u8 *row, size_t n) {
    while (n--) if (*row++) return false;
    return true;
}

void analyze(const char *name, const u8 *font, const size_t actual_size) {
    const u8 *const endp = font + actual_size;
    u8 w = *font++;
    u8 h = *font++;
    u8 offset = *font++;
    u8 numchars = *font++;

    u8 firstchar = offset;
    u8 lastchar = offset + numchars - 1;

    printf("%s: %zu bytes, %ux%u font, %d characters, from %#02x '%c' to %#02x '%c'\n",
            name, actual_size, w, h, numchars, firstchar, firstchar, lastchar, lastchar);

    const size_t total_size = numchars * (w / 8) * h;
    if (font + total_size != endp)
    {
        printf("Wrong data size: should be %zu bytes but is only %zu\n",
            4 + total_size, actual_size);
    }

    size_t min_top_pad = h, max_top_pad = 0;
    size_t min_bot_pad = h, max_bot_pad = 0;
    size_t sum_bot_pad = 0, sum_top_pad = 0;
    for (size_t i = 0; i < numchars; i++) {
        u8 bytew = w / 8;
        const u8* cp = font + bytew * h * i;
        size_t top_pad = h;
        for (size_t y = 0; y < h; y++) {
            if (!is_blank(cp + y * bytew, bytew)) {
                top_pad = y;
                break;
            }
        }
        size_t bot_pad = h - top_pad;
        for (size_t y = h; y-- > top_pad;) {
            if (!is_blank(cp + y * bytew, bytew)) {
                bot_pad = h - y - 1;
                break;
            }
        }
        sum_top_pad += top_pad * bytew;
        min_top_pad = min(min_top_pad, top_pad);
        max_top_pad = max(max_top_pad, top_pad);
        sum_bot_pad += bot_pad * bytew;
        min_bot_pad = min(min_bot_pad, bot_pad);
        max_bot_pad = max(max_bot_pad, bot_pad);
    }

    printf("Top padding    %zu..%zu rows, %zu total\n", min_top_pad, max_top_pad, sum_top_pad);
    printf("Bottom padding %zu..%zu rows, %zu total\n", min_bot_pad, max_bot_pad, sum_bot_pad);

    printf("%zu of %zu bytes used, %zu bytes padding\n", total_size - (sum_top_pad + sum_bot_pad), total_size, sum_top_pad + sum_bot_pad);
    printf("Allowed overhead %.1f bytes / character\n", double(sum_top_pad + sum_bot_pad) / numchars);

    printf("\n");
}

}

#define A(font) analyze(#font, font, sizeof font)
int main() {
    A(SmallFont);
    A(BigFont);
    A(SevenSegNumFont);
}
#endif
