# Note: Arduino SDK 1.8.3 generates 40 bytes larger code than the 1.0.5 version
# currently in Ubuntu packages.
# (OTOH, 1.0.5 generates some warnings in HardwareSerial.cpp)
ARDMK_DIR = $(CURDIR)/Arduino-Makefile
USER_LIB_PATH = $(CURDIR)/libraries

TARGET = tft_test
BOARD_TAG = uno
MONITOR_PORT = /dev/ttyACM0

# -mrelax reduced size by 142b in my test. None of the other flags I tried did
# anything.
CGOPTS = -mrelax
WARNINGS = -Wstrict-aliasing -Wall -Wextra

COMMON_CFLAGS := -gdwarf $(CGOPTS) $(WARNINGS)
CPPFLAGS += $(COMMON_CFLAGS)
LDFLAGS += $(COMMON_CFLAGS)

include $(ARDMK_DIR)/Arduino.mk
